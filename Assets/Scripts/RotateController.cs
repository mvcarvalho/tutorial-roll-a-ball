﻿using UnityEngine;

public class RotateController : MonoBehaviour {

	public float xRotation = 15;
	public float yRotation = 15;
	public float zRotation = 15;

	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(xRotation, yRotation, zRotation) * Time.deltaTime);
	}
}
