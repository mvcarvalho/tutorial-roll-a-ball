﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 10f;
	public int scoreIncrement = 1;
	public Text scoreText;
	public Text winText;

	private Rigidbody rigidbody;
	private int score;

	void Start(){
		rigidbody = GetComponent<Rigidbody>();
		score = 0;
		UpdateScoreText();
	}

	void FixedUpdate(){
		float moveVertical = Input.GetAxis("Vertical");
		float moveHorizontal = Input.GetAxis("Horizontal");

		Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
		
		rigidbody.AddForce(movement * speed);
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("pickup")){
			other.gameObject.SetActive(false);
			score += scoreIncrement;
			UpdateScoreText();
		}
	}

	private void UpdateScoreText(){
		if(score < 8)
			winText.enabled = false;
		else
			winText.enabled = true;
			
		scoreText.text = "Score: " + score.ToString();
	}
}
